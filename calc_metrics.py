import argparse
import os
import re
from metrics import MetricsProcessor, DataClass

def find_files(lst, prefix, ext):
    ans = []
    for fn in lst:
        if fn.startswith(prefix) and fn.endswith(ext):
            ans.append(fn)
    return ans


def main():
    metrics = MetricsProcessor()
    ap = argparse.ArgumentParser()
    ap.add_argument('orig', type=str, help="Path to the original YUV/PNG file")
    ap.add_argument('rec', type=str, help="Path to the reconstructed YUV/PNG file")
    ap.add_argument('bit', type=str, help="Path to the bitstream")
    ap.add_argument('-s', default="summary.txt", help="Path output file with statistics")
    ap.add_argument('--format', default="png", choices=["yuv", "png"], help="Format of input image files")
    ap.add_argument('--no-bpp', default=False, action="store_true", help="Don't calculate bpp")
    #ap.add_argument('-v', default=False, action="store_true", help="Verbose mode")
    #ap.add_argument('--csv', default=False, action="store_true", help="Output file in CSV format")
    metrics.add_arguments(ap)

    args = ap.parse_args()
    metrics.parse_arguments(ap)

    metrics.init_summary_file(args.s, os.path.splitext(args.s)[1][1:], 'a')

    data_o, target_bd = DataClass().load_image(args.orig,def_bits=metrics.internal_bits, color_conv=metrics.color_conv)
    data_r, _ = DataClass().load_image(args.rec,def_bits=target_bd, color_conv=metrics.color_conv,def_fmt=data_o.fmt)
    assert data_o.fmt == data_r.fmt
    if args.no_bpp:
        bpp = 0
    elif not os.path.exists(args.bit):
        bpp =-1
    else:
        bpp = metrics.bpp_calc(args.bit, data_r.shape)
    metrics_vals = metrics.process_images(data_o, data_r)
    complexity_data = metrics.load_complexity_info(args.rec)
            
    metrics.write_data(os.path.basename(args.rec), bpp=bpp, metrics=metrics_vals, prefix_data=None, postfix_list=complexity_data)
    metrics.close_file()


if __name__ == "__main__":
    main()